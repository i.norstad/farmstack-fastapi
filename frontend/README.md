### Project Goals:
- To demo a simple FARM stack project, using MongoDB Atlas Cloud, FastAPI, and React.

### To run this project:
- cd to backend, run: `uvicorn main:app --reload`
- then open a new terminal and run: `npm run dev`

### Next steps:
- Dockerize it into 3 containers:
* MongoDb
* API
* Client

- Add a CI/CD pipeline with tests
- Add more features/ broaden functionality
